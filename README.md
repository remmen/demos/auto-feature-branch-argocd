# feature-branch deployment


This is a proof of concept demo application which will automaticallly roll-out a new version of your application triggered by the tag `feature/<your-dev-task>` using the principals of gitops and gitflow.

## Steps

- Install argocd https://argo-cd.readthedocs.io/en/stable/getting_started
    ```
    kubectl create namespace argocd
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    ```

    You can access the UI with port-forwading
    ```
     kubectl port-forward svc/argocd-server -n argocd 8888:443
    ```

    The secrect is stored as `argocd-initial-admin-secret`, user is admin

- Create a new public gitlab repository to host the argocd application definition. This url will be used in the argocd-app.yaml

- Adjust the file argocd-apps/argocd.template to reflect your paths and commit the directory argocd-apps to the argocd application repository

- Create a public docker account to be able to use the docker registry.

- Create a docker repository. Your usernamea and repository will be used as REGISTRY_PROJECT. For example : myname/myproject

- Adjust the existing .gitlab-ci.yml configuration to your environment (paths to cache/artifacts)

- Set the following variables within your Gitlab Pipeline

| Key                  | Value                 | Description                |
|----------------------|-----------------------|----------------------------|
| `GIT_USER`           | dummy                 | but must be defined        |
| `GIT_TOKEN`          | Token                 | Need API write access      |
| `ARGOCD_GIT_URL`     | URL                   | without `https://`         |
| `ARGOCD_DEPLOY_REPO` | Name of the repo      |                            |
| `REGISTRY_PROJECT`   | Docker                |                            |
| `REGISTRY_USER`      | Docker                |                            |
| `REGISTRY_TOKEN`     | Docker                |                            |


 - Create the namespace `demo`on your cluster

 - Adjust the argocd-app.yaml to use your ArgoCD Repository URL to the cluster and verify that it is running on the UI

 - Test the workflow