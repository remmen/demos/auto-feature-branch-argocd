FROM nginx:stable-alpine
LABEL maintainer="marc@remmen.io"
LABEL app="demo"

COPY app/.output/public /usr/share/nginx/html

